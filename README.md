# Detecting conformational clusters using orthogonalizing autoencoders and Gaussian Mixture models

In this project we use autoencoders with orthogonal dimensions in the encoded space for dimensionality reduction. Gaussian Mixture Models (Bayesian) is them used to detect clustering in the reduced space. Distribution of specific functions defined over the input coordiates can then be studied as a distribution over the clusters.
